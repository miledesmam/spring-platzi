package com.platzi.curso.ereservation.service;

import com.platzi.curso.ereservation.model.Reserva;
import com.platzi.curso.ereservation.repository.ReservaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class ReservaClienteImpl implements ReservaService {

    @Autowired
    private ReservaRepository reservaRepository;

    /**
     * Crea o actualiza Reserva
     * @param reserva
     * @return
     */
    @Override
    @Transactional
    public Reserva createOrUpdate(Reserva reserva){
        return reservaRepository.save(reserva);
    }

    /**
     * Elimina Reserva
     * @param reserva
     */
    @Override
    @Transactional
    public void delete(Reserva reserva){
        reservaRepository.delete(reserva);
    }

    /**
     * Búsqueda de Reserva utilizando fecha inicio y fecha fin
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    @Override
    public List<Reserva> find(Date fechaInicio, Date fechaFin){
        return reservaRepository.find(fechaInicio, fechaFin);
    }

}
