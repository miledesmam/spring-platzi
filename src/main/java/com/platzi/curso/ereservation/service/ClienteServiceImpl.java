package com.platzi.curso.ereservation.service;

import com.platzi.curso.ereservation.model.Cliente;
import com.platzi.curso.ereservation.model.vo.ClienteVO;
import com.platzi.curso.ereservation.repository.ClienteRepository;
import com.platzi.curso.ereservation.util.ObjectMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
@Transactional(readOnly = true)
public class ClienteServiceImpl implements ClienteService {
    @Autowired
    private ObjectMapperUtil objectMapperUtil;

    @Autowired
    private ClienteRepository clienteRepository;

    /**
     * Método que realiza el guardado de un Cliente
     * @param cliente
     * @return
     */
    @Override
    @Transactional
    public void createOrUpdate(ClienteVO cliente){
        Cliente clienteEntity = objectMapperUtil.getObjectMapper().convertValue(cliente,Cliente.class);
        clienteRepository.save(clienteEntity);
    }

    @Override
    @Transactional
    public void updatePatch(ClienteVO clienteActual, ClienteVO clienteModificado){
        if(clienteActual != null && clienteModificado != null){
            Cliente clienteAGuardar = objectMapperUtil.getObjectMapper().convertValue(validateChanges(clienteActual, clienteModificado),Cliente.class);
            clienteRepository.save(clienteAGuardar);
        }
    }

    /**
     * Método que realiza la eliminación de un cliente
     * @param cliente
     */
    @Override
    @Transactional
    public void delete(ClienteVO cliente){
        clienteRepository.delete(objectMapperUtil.getObjectMapper().convertValue(cliente,Cliente.class));
    }

    /**
     * Búsqueda de Cliente por apellido
     * @param appelidoCliente
     * @return
     */
    @Override
    public ClienteVO findByApellidoCliente(String appelidoCliente){
        return objectMapperUtil.getObjectMapper().convertValue(clienteRepository.findByApellidoCliente(appelidoCliente),ClienteVO.class);
    }

    /**
     * Búsqueda de Cliente por Identificación
     * @param identificadorCliente
     * @return
     */
    @Override
    public ClienteVO findByIdentificacion(String identificadorCliente){
        return objectMapperUtil.getObjectMapper().convertValue(clienteRepository.findByIdentificadorCliente(identificadorCliente),ClienteVO.class);
    }

    @Override
    public List<ClienteVO> findAll(){
        return (List<ClienteVO>) objectMapperUtil.getObjectMapper().convertValue(clienteRepository.findAll(), ArrayList.class);
    }

    @Override
    @Transactional
    public void save(ClienteVO cliente){
        clienteRepository.save(objectMapperUtil.getObjectMapper().convertValue(cliente,Cliente.class));
    }

    @Override
    public ClienteVO findByNombreAndApellido(String nombre, String apellido){
        return objectMapperUtil.getObjectMapper().convertValue(clienteRepository.findByNombreAndApellido(nombre,apellido),ClienteVO.class);
    }

    @Override
    public ClienteVO findById(Long id){
        return objectMapperUtil.getObjectMapper().convertValue(clienteRepository.findById(id).orElse(null),ClienteVO.class);
    }

    private ClienteVO validateChanges(ClienteVO clienteActual, ClienteVO clienteNuevo){
        if(clienteNuevo.getNombreCliente() != null){
            clienteActual.setNombreCliente(clienteNuevo.getNombreCliente());
        }
        if(clienteNuevo.getApellidoCliente() != null){
            clienteActual.setApellidoCliente(clienteNuevo.getApellidoCliente());
        }
        if(clienteNuevo.getIdentificadorCliente() != null){
            clienteActual.setIdentificadorCliente(clienteNuevo.getIdentificadorCliente());
        }
        return clienteActual;
    }
}
