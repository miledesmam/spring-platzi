package com.platzi.curso.ereservation.service;

import com.platzi.curso.ereservation.model.vo.ClienteVO;
import com.platzi.curso.ereservation.repository.ClienteRepository;
import com.platzi.curso.ereservation.model.Cliente;
import com.platzi.curso.ereservation.util.ObjectMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;



public interface ClienteService {

    public void createOrUpdate(ClienteVO cliente);
    public void updatePatch(ClienteVO clienteActual, ClienteVO clienteModificado);
    public void delete(ClienteVO cliente);
    public ClienteVO findByApellidoCliente(String appelidoCliente);
    public ClienteVO findByIdentificacion(String identificadorCliente);
    public List<ClienteVO> findAll();
    public void save(ClienteVO cliente);
    public ClienteVO findByNombreAndApellido(String nombre, String apellido);
    public ClienteVO findById(Long id);

}
