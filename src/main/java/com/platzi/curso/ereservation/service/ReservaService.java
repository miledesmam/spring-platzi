package com.platzi.curso.ereservation.service;

import com.platzi.curso.ereservation.repository.ReservaRepository;
import com.platzi.curso.ereservation.model.Reserva;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


public interface ReservaService {
    public Reserva createOrUpdate(Reserva reserva);
    public void delete(Reserva reserva);
    public List<Reserva> find(Date fechaInicio, Date fechaFin);
}
