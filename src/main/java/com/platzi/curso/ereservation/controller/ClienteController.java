package com.platzi.curso.ereservation.controller;

import com.platzi.curso.ereservation.service.ClienteService;
import com.platzi.curso.ereservation.model.Cliente;
import com.platzi.curso.ereservation.model.vo.ClienteVO;
import com.platzi.curso.ereservation.util.CustomErrorType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


/**
 * Controller con metodos crud de Api Rest
 */
@RestController
@RequestMapping("/v1")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    //GET
    @ResponseBody
    @GetMapping(value = "/clientes",headers = "Accept=Application/json")
    public ResponseEntity<List<ClienteVO>> getClientes(@RequestParam(value = "apellidoCliente",required = false)String apellido,
                                                     @RequestParam(value = "nombreCliente",required = false) String nombre){
        List<ClienteVO> clientes = new ArrayList<>();

        if(apellido == null && nombre == null) {

            clientes = clienteService.findAll();

            if (clientes.isEmpty()) {
                return new ResponseEntity(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(clientes, HttpStatus.OK);
        }else if(apellido != null){
            ClienteVO cliente = clienteService.findByApellidoCliente(apellido);
            if(cliente==null){
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
            clientes.add(cliente);
            return new ResponseEntity<>(clientes, HttpStatus.OK);

        }else {
            ClienteVO cliente = clienteService.findByNombreAndApellido(nombre,apellido);
            if(cliente==null){
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
            clientes.add(cliente);
            return new ResponseEntity<>(clientes, HttpStatus.OK);
        }
    }

    //GET
    @GetMapping(value = "/clientes/{id}", headers = "Accept=application/json")
    public ResponseEntity<ClienteVO>getClienteById(@PathVariable("id") Long idCliente){
        if(idCliente == null || idCliente <= 0){
            return new ResponseEntity(new CustomErrorType("idCliente es requerido"),HttpStatus.CONFLICT);
        }
        ClienteVO cliente = clienteService.findById(idCliente);
        if(cliente == null){
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity(cliente, HttpStatus.OK);
    }

    //POST
    @PostMapping(value = "/clientes", headers = "Accept=application/json")
    public ResponseEntity createCliente(@Valid @RequestBody ClienteVO cliente, UriComponentsBuilder uriComponentsBuilder){
        if(cliente.getNombreCliente().isEmpty() || cliente.getApellidoCliente().isEmpty()){
            return new ResponseEntity(new CustomErrorType("nombreCliente es requerido"),HttpStatus.CONFLICT);
        }
        if(clienteService.findByNombreAndApellido(cliente.getNombreCliente(),cliente.getApellidoCliente())!= null){
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        clienteService.save(cliente);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(uriComponentsBuilder.path("/v1/clientes/{id}").
                buildAndExpand(1)
                .toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    //UPDATE
    @PutMapping(value = "clientes/{id}", headers = "Accept=application/json")
    public ResponseEntity updateClientePut(@PathVariable("id") Long idCliente, @RequestBody ClienteVO cliente){
        if(idCliente == null || idCliente <= 0){
            return new ResponseEntity(new CustomErrorType("idCliente es requerido."),HttpStatus.CONFLICT);
        }
        ClienteVO currentCliente = clienteService.findById(idCliente);
        if(currentCliente == null){
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }

        clienteService.createOrUpdate(cliente);
        return new ResponseEntity(currentCliente,HttpStatus.OK);
    }

    //Update parcial
    @PatchMapping(value = "clientes/{id}", headers = "Accept=application/json")
    public ResponseEntity updateClientePatch(@PathVariable("id") Long idCliente,  @RequestBody ClienteVO cliente){
        if(idCliente == null || idCliente <= 0){
            return new ResponseEntity(new CustomErrorType("idCliente es_requerido."),HttpStatus.CONFLICT);
        }
        ClienteVO currentCliente = clienteService.findById(idCliente);
        if(currentCliente == null){
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }

        clienteService.updatePatch(currentCliente, cliente);
        return new ResponseEntity(cliente,HttpStatus.OK);
    }

    //DELETE
    @DeleteMapping(value = "clientes/{id}",  headers = "Accept=application/json")
    public ResponseEntity deleteCliente(@PathVariable("id") Long idCliente){
        if(idCliente == null || idCliente <= 0){
            return new ResponseEntity(new CustomErrorType("idCliente es requerido-"),HttpStatus.CONFLICT);
        }
        ClienteVO currentCliente = clienteService.findById(idCliente);
        if(currentCliente == null){
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }

        clienteService.delete(currentCliente);

        return new ResponseEntity<Cliente>(HttpStatus.OK);

    }
}
