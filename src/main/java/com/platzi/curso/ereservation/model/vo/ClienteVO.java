package com.platzi.curso.ereservation.model.vo;

import lombok.Data;

import java.io.Serializable;


@Data
public class ClienteVO implements Serializable {

    private static final long serialVersionUID = 8835553836834071951L;
    private Long id;
    private String nombreCliente;
    private String apellidoCliente;
    private String identificadorCliente;

    public ClienteVO() {
    }

    public ClienteVO(String nombreCliente, String apellidoCliente, String identificadorCliente) {
        this.nombreCliente = nombreCliente;
        this.apellidoCliente = apellidoCliente;
        this.identificadorCliente = identificadorCliente;
    }



    @Override
    public String toString() {
        return "ClienteVO{" +
                "id=" + id +
                ", nombreCliente='" + nombreCliente + '\'' +
                ", apellidoCliente='" + apellidoCliente + '\'' +
                ", identificadorCliente='" + identificadorCliente + '\'' +
                '}';
    }
}
