package com.platzi.curso.ereservation.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * Entidad Reserva.
 * Mapea tabla Reservas
 * @author Mario Ledesma
 */

@Data
@Entity
@Table(name = "reservas")
public class Reserva implements Serializable {

    private static final long serialVersionUID = 6519628621696081678L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Date fechaIngresoReserva;
    private Date fechaSalidaReserva;
    private Integer cantidadPersonasReserva;
    private String descripcionReserva;

    @ManyToOne
    @JoinColumn(name = "idCliente")
    private Cliente cliente;

    @Override
    public String toString() {
        return "Reserva{" +
                "id=" + id +
                ", fechaIngresoReserva=" + fechaIngresoReserva +
                ", fechaSalidaReserva=" + fechaSalidaReserva +
                ", cantidadPersonasReserva=" + cantidadPersonasReserva +
                ", descripcionReserva='" + descripcionReserva + '\'' +
                ", cliente=" + cliente +
                '}';
    }
}
