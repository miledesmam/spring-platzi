package com.platzi.curso.ereservation.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

/**
 * Entidad Cliente.
 * Mapea tabla cliente
 * @author Mario Ledesma
 */
@Entity
@Table(name = "clientes")
@Data
@NamedQuery(name = "Cliente.findByIdentificacion",query = "select c from Cliente c where c.identificadorCliente = ?1")
public class Cliente implements Serializable {

    private static final long serialVersionUID = -7120557490131715299L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nombreCliente;
    private String apellidoCliente;
    private String identificadorCliente;

    @JsonIgnore
    @OneToMany(mappedBy = "cliente")
    private Set<Reserva> reservas;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return Objects.equals(id, cliente.id) &&
                Objects.equals(nombreCliente, cliente.nombreCliente) &&
                Objects.equals(apellidoCliente, cliente.apellidoCliente) &&
                Objects.equals(identificadorCliente, cliente.identificadorCliente) &&
                Objects.equals(reservas, cliente.reservas);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombreCliente, apellidoCliente, identificadorCliente, reservas);
    }

    public Cliente() {
    }

    public Cliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public Cliente(String nombreCliente, String apellidoCliente) {
        this.nombreCliente = nombreCliente;
        this.apellidoCliente = apellidoCliente;
    }

    public Cliente(String nombreCliente, String apellidoCliente, String identificadorCliente) {
        this.nombreCliente = nombreCliente;
        this.apellidoCliente = apellidoCliente;
        this.identificadorCliente = identificadorCliente;
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "id=" + id +
                ", nombreCliente='" + nombreCliente + '\'' +
                ", apellidoCliente='" + apellidoCliente + '\'' +
                ", identificadorCliente='" + identificadorCliente + '\'' +
                '}';
    }
}
