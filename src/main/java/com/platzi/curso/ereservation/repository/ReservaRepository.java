package com.platzi.curso.ereservation.repository;

import com.platzi.curso.ereservation.model.Reserva;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface ReservaRepository extends JpaRepository<Reserva, Long> {
    @Query("Select r from Reserva r where r.fechaIngresoReserva = :fechaInicio and r.fechaSalidaReserva = :fechaSalida")
    public List<Reserva> find(@Param("fechaInicio")Date fechaInicio, @Param("fechaSalida") Date fechaSalida);
}
