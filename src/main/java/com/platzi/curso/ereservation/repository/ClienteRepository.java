package com.platzi.curso.ereservation.repository;

import com.platzi.curso.ereservation.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
    /**
     * Define el método de búsqueda de clientes por apellido
     * @param appelidoCliente
     * @return
     */
    public Cliente findByApellidoCliente(String appelidoCliente);

    public Cliente findByIdentificadorCliente(String identificadorCliente);

    @Query("select c from Cliente c where c.nombreCliente = :nCliente and c.apellidoCliente = :apCliente")
    public Cliente findByNombreAndApellido(@Param("nCliente") String nombreCliente,
                                           @Param("apCliente") String apellidoCliente);

    public Cliente findByNombreClienteAndApellidoCliente(String nombreCliente, String apellidoCliente);
}
